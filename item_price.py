import json
import struct

from consts import Item2Name, Name2Item
from main import BinFile, INDENT, ITEM_COUNT

class ItemPricesFile(BinFile):
    blocks: list[int]

    def __init__(self):
        self.block_size = 0x4
        self.blocks = []
    
    def saveToBin(self, filepath) -> None:
        with open(filepath, "wb") as fout:
            fout.write(self.getHeader())
            for price in self.blocks:
                fout.write(struct.pack("<I", price))
    
    def saveToJson(self, filepath) -> None:
        with open(filepath, "w") as fout:
            outJson = { "prices": [] }
            self.saveHeaderToJson(outJson)

            for (i, price) in enumerate(self.blocks):
                itemId = i + 0x2000
                if Item2Name(itemId) == None:
                    continue
                outJson["prices"].append({"item": Item2Name(itemId), "price": price})
            
            fout.write(json.dumps(outJson, indent=INDENT))
    
    def loadFromBin(self, filepath) -> None:
        with open(filepath, "rb") as fin:
            byts = fin.read()
            self.readHeaderFromBin(byts)
            self.blocks.clear()
            for (i, (price,)) in enumerate(struct.iter_unpack("<I", byts[self.getBlocksStartAddress():])):
                if i > ITEM_COUNT:
                    break
                self.blocks.append(price)
    
    def loadFromJson(self, filepath) -> None:
        with open(filepath, "r") as fin:
            inJson = json.load(fin)
            self.readHeaderFromJson(inJson)
            inJson["prices"].sort(key=lambda e: Name2Item(e["item"]).value)
            self.blocks.clear()
            for item in inJson["prices"]:
                self.blocks.append(item["price"])
