import json
import struct
from dataclasses import dataclass

from consts import *
from main import BinFile, INDENT

@dataclass
class CustomizationEntry:
    appliesTo: int
    autoAbilityId: int
    itemId: int
    itemCount: int

class CustomizationFile(BinFile):
    blocks: list[CustomizationEntry]

    def __init__(self):
        self.block_size = 0x8
        self.blocks = []
    
    def sort(self):
        self.blocks.sort(key=lambda e: e.autoAbilityId)

    def saveToBin(self, filepath):
        with open(filepath, "wb") as fout:
            fout.write(self.getHeader())
            for c in self.blocks:
                fout.write(struct.pack("<HHHH", c.appliesTo, c.autoAbilityId, c.itemId, c.itemCount))
    
    def saveToJson(self, filepath):
        with open(filepath, "w") as fout:
            outJson = { "customizations": [] }
            self.saveHeaderToJson(outJson)

            for c in self.blocks:
                appliesTo = "none"
                match c.appliesTo & 0x3:
                    case 0x1: appliesTo = "weapons"
                    case 0x2: appliesTo = "armor"
                    case 0x3: appliesTo = "both"
                outJson["customizations"].append({
                    "appliesTo": appliesTo,
                    "autoAbility": AutoAbility2Name(c.autoAbilityId),
                    "item": Item2Name(c.itemId),
                    "itemCount": c.itemCount
                })
            fout.write(json.dumps(outJson, indent=INDENT))

    def loadFromBin(self, filepath):
        with open(filepath, "rb") as fin:
            byts = fin.read()
            self.readHeaderFromBin(byts)
            self.blocks.clear()
            for c in struct.iter_unpack("<HHHH", byts[0x14:]):
                (appliesTo, autoAbilityId, itemId, itemCount) = c
                self.blocks.append(CustomizationEntry(appliesTo, autoAbilityId, itemId, itemCount))
        self.sort()
    
    def loadFromJson(self, filepath):
        with open(filepath, "r") as fin:
            inJson = json.load(fin)
            self.readHeaderFromJson(inJson)

            self.blocks.clear()
            for c in inJson["customizations"]:
                appliesTo = 0x0
                match c["appliesTo"]:
                    case "none": appliesTo = 0x0
                    case "weapons": appliesTo = 0x1
                    case "armor": appliesTo = 0x2
                    case "both": appliesTo = 0x3
                
                autoAbilityId = Name2AutoAbility(c["autoAbility"])
                itemId = Name2Item(c["item"])
                itemCount = c["itemCount"]

                self.blocks.append(CustomizationEntry(appliesTo, autoAbilityId, itemId, itemCount))
        self.sort()