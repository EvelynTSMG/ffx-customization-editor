import json
import struct
from dataclasses import dataclass

from consts import Items, Item2Name, Name2Item
from main import BinFile, INDENT

@dataclass
class ItemShopEntry:
    deprecated_shopRatio: int
    items: list[Items]

class ItemShopsFile(BinFile):
    blocks: list[ItemShopEntry]

    def __init__(self):
        self.block_size = 0x22
        self.blocks = []
    
    def saveToBin(self, filepath):
        with open(filepath, "wb") as fout:
            fout.write(self.getHeader())
            for s in self.blocks:
                fout.write(struct.pack("<H", s.deprecated_shopRatio))
                for i in range(0, 16):
                    itemId = Items.NULL
                    if i < len(s.items):
                        itemId = s.items[i]
                    fout.write(struct.pack("<H", itemId))

    def saveToJson(self, filepath) -> None:
        with open(filepath, "w") as fout:
            outJson = { "shops": [] }
            self.saveHeaderToJson(outJson)

            for s in self.blocks:
                outJson["shops"].append({
                    "DEPRECATED shopRatio": s.deprecated_shopRatio,
                    "items": [Item2Name(item) for item in s.items]
                })
            
            fout.write(json.dumps(outJson, indent=INDENT))
    
    def loadFromBin(self, filepath) -> None:
        with open(filepath, "rb") as fin:
            byts = fin.read()
            self.readHeaderFromBin(byts)
            self.blocks.clear()
            for s in struct.iter_unpack("<HHHHHHHHHHHHHHHHH", byts[self.getBlocksStartAddress():]):
                a, *b = s
                self.blocks.append(ItemShopEntry(a, [item for item in b if item != 0x0]))
    
    def loadFromJson(self, filepath) -> None:
        with open(filepath, "r") as fin:
            inJson = json.load(fin)
            self.readHeaderFromJson(inJson)
            self.blocks.clear()
            for shop in inJson["shops"]:
                self.blocks.append(ItemShopEntry(shop["DEPRECATED shopRatio"], [Name2Item(item) for item in shop["items"]]))

