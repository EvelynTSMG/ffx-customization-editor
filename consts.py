from enum import IntEnum

class Items(IntEnum):
    NULL = 0x00
    NONE = 0xFF
    POTION = 0x2000
    HI_POTION = 0x2001
    X_POTION = 0x2002
    MEGA_POTION = 0x2003
    ETHER = 0x2004
    TURBO_ETHER = 0x2005
    PHOENIX_DOWN = 0x2006
    MEGA_PHOENIX = 0x2007
    ELIXIR = 0x2008
    MEGALIXIR = 0x2009
    ANTIDOTE = 0x200A
    SOFT = 0x200B
    EYE_DROPS = 0x200C
    ECHO_SCREEN = 0x200D
    HOLY_WATER = 0x200E
    REMEDY = 0x200F
    POWER_DISTILLER = 0x2010
    MANA_DISTILLER = 0x2011
    SPEED_DISTILLER = 0x2012
    ABILITY_DISTILLER = 0x2013
    ALBHED_POTION = 0x2014
    HEALING_WATER = 0x2015
    TETRA_ELEMENTAL = 0x2016
    ANTARCTIC_WIND = 0x2017
    ARCTIC_WIND = 0x2018
    ICE_GEM = 0x2019
    BOMB_FRAGMENT = 0x201A
    BOMB_CORE = 0x201B
    FIRE_GEM = 0x201C
    ELECTRO_MARBLE = 0x201D
    LIGHTNING_MARBLE = 0x201E
    LIGHTNING_GEM = 0x201F
    FISH_SCALE = 0x2020
    DRAGON_SCALE = 0x2021
    WATER_GEM = 0x2022
    GRENADE = 0x2023
    FRAG_GRENADE = 0x2024
    SLEEPING_POWDER = 0x2025
    DREAM_POWDER = 0x2026
    SILENCE_GRENADE = 0x2027
    SMOKE_BOMB = 0x2028
    SHADOW_GEM = 0x2029
    SHINING_GEM = 0x202A
    BLESSED_GEM = 0x202B
    SUPREME_GEM = 0x202C
    POISON_FANG = 0x202D
    SILVER_HOURGLASS = 0x202E
    GOLD_HOURGLASS = 0x202F
    CANDLE_OF_LIFE = 0x2030
    PETRIFY_GRENADE = 0x2031
    FARPLANE_SHADOW = 0x2032
    FARPLANE_WIND = 0x2033
    DESIGNER_WALLET = 0x2034
    DARK_MATTER = 0x2035
    CHOCOBO_FEATHER = 0x2036
    CHOCOBO_WING = 0x2037
    LUNAR_CURTAIN = 0x2038
    LIGHT_CURTAIN = 0x2039
    STAR_CURTAIN = 0x203A
    HEALING_SPRING = 0x203B
    MANA_SPRING = 0x203C
    STAMINA_SPRING = 0x203D
    SOUL_SPRING = 0x203E
    PURIFYING_SALT = 0x203F
    STAMINA_TABLET = 0x2040
    MANA_TABLET = 0x2041
    TWIN_STARS = 0x2042
    STAMINA_TONIC = 0x2043
    MANA_TONIC = 0x2044
    THREE_STARS = 0x2045
    POWER_SPHERE = 0x2046
    MANA_SPHERE = 0x2047
    SPEED_SPHERE = 0x2048
    ABILITY_SPHERE = 0x2049
    FORTUNE_SPHERE = 0x204A
    ATTRIBUTE_SPHERE = 0x204B
    SPECIAL_SPHERE = 0x204C
    SKILL_SPHERE = 0x204D
    WHT_MAGIC_SPHERE = 0x204E
    BLK_MAGIC_SPHERE = 0x204F
    MASTER_SPHERE = 0x2050
    LV1_KEY_SPHERE = 0x2051
    LV2_KEY_SPHERE = 0x2052
    LV3_KEY_SPHERE = 0x2053
    LV4_KEY_SPHERE = 0x2054
    HP_SPHERE = 0x2055
    MP_SPHERE = 0x2056
    STRENGTH_SPHERE = 0x2057
    DEFENSE_SPHERE = 0x2058
    MAGIC_SPHERE = 0x2059
    MAGIC_DEF_SPHERE = 0x205A
    AGILITY_SPHERE = 0x205B
    EVASION_SPHERE = 0x205C
    ACCURACY_SPHERE = 0x205D
    LUCK_SPHERE = 0x205E
    CLEAR_SPHERE = 0x205F
    RETURN_SPHERE = 0x2060
    FRIEND_SPHERE = 0x2061
    TELEPORT_SPHERE = 0x2062
    WARP_SPHERE = 0x2063
    MAP = 0x2064
    RENAME_CARD = 0x2065
    MUSK = 0x2066
    HYPELLO_POTION = 0x2067
    SHINING_THORN = 0x2068
    PENDULUM = 0x2069
    AMULET = 0x206A
    DOOR_TO_TOMORROW = 0x206B
    WINGS_TO_DISCOVERY = 0x206C
    GAMBLERS_SPIRIT = 0x206D
    UNDERDOGS_SECRET = 0x206E
    WINNING_FORMULA = 0x206F

def Item2Name(item: Items) -> str:
    match item:
        case Items.NULL: return ""
        case Items.NONE: return "None"
        case Items.POTION: return "Potion"
        case Items.HI_POTION: return "Hi-Potion"
        case Items.X_POTION: return "X-Potion"
        case Items.MEGA_POTION: return "Mega-Potion"
        case Items.ETHER: return "Ether"
        case Items.TURBO_ETHER: return "Turbo Ether"
        case Items.PHOENIX_DOWN: return "Phoenix Down"
        case Items.MEGA_PHOENIX: return "Mega-Phoenix"
        case Items.ELIXIR: return "Elixir"
        case Items.MEGALIXIR: return "Megalixir"
        case Items.ANTIDOTE: return "Antidote"
        case Items.SOFT: return "Soft"
        case Items.EYE_DROPS: return "Eye Drops"
        case Items.ECHO_SCREEN: return "Echo Screen"
        case Items.HOLY_WATER: return "Holy Water"
        case Items.REMEDY: return "Remedy"
        case Items.POWER_DISTILLER: return "Power Distiller"
        case Items.MANA_DISTILLER: return "Mana Distiller"
        case Items.SPEED_DISTILLER: return "Speed Distiller"
        case Items.ABILITY_DISTILLER: return "Ability Distiller"
        case Items.ALBHED_POTION: return "Al-Bhed Potion"
        case Items.HEALING_WATER: return "Healing Water"
        case Items.TETRA_ELEMENTAL: return "Tetra Elemental"
        case Items.ANTARCTIC_WIND: return "Antarctic Wind"
        case Items.ARCTIC_WIND: return "Arctic Wind"
        case Items.ICE_GEM: return "Ice Gem"
        case Items.BOMB_FRAGMENT: return "Bomb Fragment"
        case Items.BOMB_CORE: return "Bomb Core"
        case Items.FIRE_GEM: return "Fire Gem"
        case Items.ELECTRO_MARBLE: return "Electro Marble"
        case Items.LIGHTNING_MARBLE: return "Lightning Marble"
        case Items.LIGHTNING_GEM: return "Lightning Gem"
        case Items.FISH_SCALE: return "Fish Scale"
        case Items.DRAGON_SCALE: return "Dragon Scale"
        case Items.WATER_GEM: return "Water Gem"
        case Items.GRENADE: return "Grenade"
        case Items.FRAG_GRENADE: return "Frag Grenade"
        case Items.SLEEPING_POWDER: return "Sleeping Powder"
        case Items.DREAM_POWDER: return "Dream Powder"
        case Items.SILENCE_GRENADE: return "Silence Grenade"
        case Items.SMOKE_BOMB: return "Smoke Bomb"
        case Items.SHADOW_GEM: return "Shadow Gem"
        case Items.SHINING_GEM: return "Shining Gem"
        case Items.BLESSED_GEM: return "Blessed Gem"
        case Items.SUPREME_GEM: return "Supreme Gem"
        case Items.POISON_FANG: return "Poison Fang"
        case Items.SILVER_HOURGLASS: return "Silver Hourglass"
        case Items.GOLD_HOURGLASS: return "Gold Hourglass"
        case Items.CANDLE_OF_LIFE: return "Candle of Life"
        case Items.PETRIFY_GRENADE: return "Petrify Grenade"
        case Items.FARPLANE_SHADOW: return "Farplane Shadow"
        case Items.FARPLANE_WIND: return "Farplane Wind"
        case Items.DESIGNER_WALLET: return "Designer Wallet"
        case Items.DARK_MATTER: return "Dark Matter"
        case Items.CHOCOBO_FEATHER: return "Chocobo Feather"
        case Items.CHOCOBO_WING: return "Chocobo Wing"
        case Items.LUNAR_CURTAIN: return "Lunar Curtain"
        case Items.LIGHT_CURTAIN: return "Light Curtain"
        case Items.STAR_CURTAIN: return "Star Curtain"
        case Items.HEALING_SPRING: return "Healing Spring"
        case Items.MANA_SPRING: return "Mana Spring"
        case Items.STAMINA_SPRING: return "Stamina Spring"
        case Items.SOUL_SPRING: return "Soul Spring"
        case Items.PURIFYING_SALT: return "Purifying Salt"
        case Items.STAMINA_TABLET: return "Stamina Tablet"
        case Items.MANA_TABLET: return "Mana Tablet"
        case Items.TWIN_STARS: return "Twin Stars"
        case Items.STAMINA_TONIC: return "Stamina Tonic"
        case Items.MANA_TONIC: return "Mana Tonic"
        case Items.THREE_STARS: return "Three Stars"
        case Items.POWER_SPHERE: return "Power Sphere"
        case Items.MANA_SPHERE: return "Mana Sphere"
        case Items.SPEED_SPHERE: return "Speed Sphere"
        case Items.ABILITY_SPHERE: return "Ability Sphere"
        case Items.FORTUNE_SPHERE: return "Fortune Sphere"
        case Items.ATTRIBUTE_SPHERE: return "Attribute Sphere"
        case Items.SPECIAL_SPHERE: return "Special Sphere"
        case Items.SKILL_SPHERE: return "Skill Sphere"
        case Items.WHT_MAGIC_SPHERE: return "Wht Magic Sphere"
        case Items.BLK_MAGIC_SPHERE: return "Blk Magic Sphere"
        case Items.MASTER_SPHERE: return "Master Sphere"
        case Items.LV1_KEY_SPHERE: return "Lv. 1 Key Sphere"
        case Items.LV2_KEY_SPHERE: return "Lv. 2 Key Sphere"
        case Items.LV3_KEY_SPHERE: return "Lv. 3 Key Sphere"
        case Items.LV4_KEY_SPHERE: return "Lv. 4 Key Sphere"
        case Items.HP_SPHERE: return "HP Sphere"
        case Items.MP_SPHERE: return "MP Sphere"
        case Items.STRENGTH_SPHERE: return "Strength Sphere"
        case Items.DEFENSE_SPHERE: return "Defense Sphere"
        case Items.MAGIC_SPHERE: return "Magic Sphere"
        case Items.MAGIC_DEF_SPHERE: return "Magic Def Sphere"
        case Items.AGILITY_SPHERE: return "Agility Sphere"
        case Items.EVASION_SPHERE: return "Evasion Sphere"
        case Items.ACCURACY_SPHERE: return "Accuracy Sphere"
        case Items.LUCK_SPHERE: return "Luck Sphere"
        case Items.CLEAR_SPHERE: return "Clear Sphere"
        case Items.RETURN_SPHERE: return "Return Sphere"
        case Items.FRIEND_SPHERE: return "Friend Sphere"
        case Items.TELEPORT_SPHERE: return "Teleport Sphere"
        case Items.WARP_SPHERE: return "Warp Sphere"
        case Items.MAP: return "Map"
        case Items.RENAME_CARD: return "Rename Card"
        case Items.MUSK: return "Musk"
        case Items.HYPELLO_POTION: return "Hypello Potion"
        case Items.SHINING_THORN: return "Shining Thorn"
        case Items.PENDULUM: return "Pendulum"
        case Items.AMULET: return "Amulet"
        case Items.DOOR_TO_TOMORROW: return "Door to Tomorrow"
        case Items.WINGS_TO_DISCOVERY: return "Wings to Discovery"
        case Items.GAMBLERS_SPIRIT: return "Gambler's Spirit"
        case Items.UNDERDOGS_SECRET: return "Underdog's Secret"
        case Items.WINNING_FORMULA: return "Winning Formula"

def Name2Item(name: str) -> Items:
    match name:
        case "": return Items.NULL
        case "None": return Items.NONE
        case "Potion": return Items.POTION
        case "Hi-Potion": return Items.HI_POTION
        case "X-Potion": return Items.X_POTION
        case "Mega-Potion": return Items.MEGA_POTION
        case "Ether": return Items.ETHER
        case "Turbo Ether": return Items.TURBO_ETHER
        case "Phoenix Down": return Items.PHOENIX_DOWN
        case "Mega-Phoenix": return Items.MEGA_PHOENIX
        case "Elixir": return Items.ELIXIR
        case "Megalixir": return Items.MEGALIXIR
        case "Antidote": return Items.ANTIDOTE
        case "Soft": return Items.SOFT
        case "Eye Drops": return Items.EYE_DROPS
        case "Echo Screen": return Items.ECHO_SCREEN
        case "Holy Water": return Items.HOLY_WATER
        case "Remedy": return Items.REMEDY
        case "Power Distiller": return Items.POWER_DISTILLER
        case "Mana Distiller": return Items.MANA_DISTILLER
        case "Speed Distiller": return Items.SPEED_DISTILLER
        case "Ability Distiller": return Items.ABILITY_DISTILLER
        case "Al-Bhed Potion": return Items.ALBHED_POTION
        case "Healing Water": return Items.HEALING_WATER
        case "Tetra Elemental": return Items.TETRA_ELEMENTAL
        case "Antarctic Wind": return Items.ANTARCTIC_WIND
        case "Arctic Wind": return Items.ARCTIC_WIND
        case "Ice Gem": return Items.ICE_GEM
        case "Bomb Fragment": return Items.BOMB_FRAGMENT
        case "Bomb Core": return Items.BOMB_CORE
        case "Fire Gem": return Items.FIRE_GEM
        case "Electro Marble": return Items.ELECTRO_MARBLE
        case "Lightning Marble": return Items.LIGHTNING_MARBLE
        case "Lightning Gem": return Items.LIGHTNING_GEM
        case "Fish Scale": return Items.FISH_SCALE
        case "Dragon Scale": return Items.DRAGON_SCALE
        case "Water Gem": return Items.WATER_GEM
        case "Grenade": return Items.GRENADE
        case "Frag Grenade": return Items.FRAG_GRENADE
        case "Sleeping Powder": return Items.SLEEPING_POWDER
        case "Dream Powder": return Items.DREAM_POWDER
        case "Silence Grenade": return Items.SILENCE_GRENADE
        case "Smoke Bomb": return Items.SMOKE_BOMB
        case "Shadow Gem": return Items.SHADOW_GEM
        case "Shining Gem": return Items.SHINING_GEM
        case "Blessed Gem": return Items.BLESSED_GEM
        case "Supreme Gem": return Items.SUPREME_GEM
        case "Poison Fang": return Items.POISON_FANG
        case "Silver Hourglass": return Items.SILVER_HOURGLASS
        case "Gold Hourglass": return Items.GOLD_HOURGLASS
        case "Candle of Life": return Items.CANDLE_OF_LIFE
        case "Petrify Grenade": return Items.PETRIFY_GRENADE
        case "Farplane Shadow": return Items.FARPLANE_SHADOW
        case "Farplane Wind": return Items.FARPLANE_WIND
        case "Designer Wallet": return Items.DESIGNER_WALLET
        case "Dark Matter": return Items.DARK_MATTER
        case "Chocobo Feather": return Items.CHOCOBO_FEATHER
        case "Chocobo Wing": return Items.CHOCOBO_WING
        case "Lunar Curtain": return Items.LUNAR_CURTAIN
        case "Light Curtain": return Items.LIGHT_CURTAIN
        case "Star Curtain": return Items.STAR_CURTAIN
        case "Healing Spring": return Items.HEALING_SPRING
        case "Mana Spring": return Items.MANA_SPRING
        case "Stamina Spring": return Items.STAMINA_SPRING
        case "Soul Spring": return Items.SOUL_SPRING
        case "Purifying Salt": return Items.PURIFYING_SALT
        case "Stamina Tablet": return Items.STAMINA_TABLET
        case "Mana Tablet": return Items.MANA_TABLET
        case "Twin Stars": return Items.TWIN_STARS
        case "Stamina Tonic": return Items.STAMINA_TONIC
        case "Mana Tonic": return Items.MANA_TONIC
        case "Three Stars": return Items.THREE_STARS
        case "Power Sphere": return Items.POWER_SPHERE
        case "Mana Sphere": return Items.MANA_SPHERE
        case "Speed Sphere": return Items.SPEED_SPHERE
        case "Ability Sphere": return Items.ABILITY_SPHERE
        case "Fortune Sphere": return Items.FORTUNE_SPHERE
        case "Attribute Sphere": return Items.ATTRIBUTE_SPHERE
        case "Special Sphere": return Items.SPECIAL_SPHERE
        case "Skill Sphere": return Items.SKILL_SPHERE
        case "Wht Magic Sphere": return Items.WHT_MAGIC_SPHERE
        case "Blk Magic Sphere": return Items.BLK_MAGIC_SPHERE
        case "Master Sphere": return Items.MASTER_SPHERE
        case "Lv. 1 Key Sphere": return Items.LV1_KEY_SPHERE
        case "Lv. 2 Key Sphere": return Items.LV2_KEY_SPHERE
        case "Lv. 3 Key Sphere": return Items.LV3_KEY_SPHERE
        case "Lv. 4 Key Sphere": return Items.LV4_KEY_SPHERE
        case "HP Sphere": return Items.HP_SPHERE
        case "MP Sphere": return Items.MP_SPHERE
        case "Strength Sphere": return Items.STRENGTH_SPHERE
        case "Defense Sphere": return Items.DEFENSE_SPHERE
        case "Magic Sphere": return Items.MAGIC_SPHERE
        case "Magic Def Sphere": return Items.MAGIC_DEF_SPHERE
        case "Agility Sphere": return Items.AGILITY_SPHERE
        case "Evasion Sphere": return Items.EVASION_SPHERE
        case "Accuracy Sphere": return Items.ACCURACY_SPHERE
        case "Luck Sphere": return Items.LUCK_SPHERE
        case "Clear Sphere": return Items.CLEAR_SPHERE
        case "Return Sphere": return Items.RETURN_SPHERE
        case "Friend Sphere": return Items.FRIEND_SPHERE
        case "Teleport Sphere": return Items.TELEPORT_SPHERE
        case "Warp Sphere": return Items.WARP_SPHERE
        case "Map": return Items.MAP
        case "Rename Card": return Items.RENAME_CARD
        case "Musk": return Items.MUSK
        case "Hypello Potion": return Items.HYPELLO_POTION
        case "Shining Thorn": return Items.SHINING_THORN
        case "Pendulum": return Items.PENDULUM
        case "Amulet": return Items.AMULET
        case "Door to Tomorrow": return Items.DOOR_TO_TOMORROW
        case "Wings to Discovery": return Items.WINGS_TO_DISCOVERY
        case "Gambler's Spirit": return Items.GAMBLERS_SPIRIT
        case "Underdog's Secret": return Items.UNDERDOGS_SECRET
        case "Winning Formula": return Items.WINNING_FORMULA

class AutoAbilities(IntEnum):
    SENSOR = 0x8000
    FIRST_STRIKE = 0x8001
    INITIATIVE = 0x8002
    COUNTERATTACK = 0x8003
    EVADE_AND_COUNTER = 0x8004
    MAGIC_COUNTER = 0x8005
    MAGIC_BOOSTER = 0x8006
    ALCHEMY = 0x8007
    AUTO_POTION = 0x8008
    AUTO_MED = 0x8009
    AUTO_PHOENIX = 0x800A
    PIERCING = 0x800B
    HALF_MP_COST = 0x800C
    ONE_MP_COST = 0x800D
    DOUBLE_OVERDRIVE = 0x800E
    TRIPLE_OVERDRIVE = 0x800F
    SOS_OVERDRIVE = 0x8010
    OVERDRIVE_TO_AP = 0x8011
    DOUBLE_AP = 0x8012
    TRIPLE_AP = 0x8013
    PICKPOCKET = 0x8015
    MASTER_THIEF = 0x8016
    BREAK_HP_LIMIT = 0x8017
    BREAK_MP_LIMIT = 0x8018
    RIBBON = 0x8019
    GILLIONAIRE = 0x801A
    HP_STROLL = 0x801B
    MP_STROLL = 0x801C
    NO_ENCOUNTERS = 0x801D
    FIRESTRIKE = 0x801E
    FIRE_WARD = 0x801F
    FIREPROOF = 0x8020
    FIRE_EATER = 0x8021
    ICESTRIKE = 0x8022
    ICE_WARD = 0x8023
    ICEPROOF = 0x8024
    ICE_EATER = 0x8025
    LIGHTNINGSTRIKE = 0x8026
    LIGHTNING_WARD = 0x8027
    LIGHTNINGPROOF = 0x8028
    LIGHTNING_EATER = 0x8029
    WATERSTRIKE = 0x802A
    WATER_WARD = 0x802B
    WATERPROOF = 0x802C
    WATER_EATER = 0x802D
    DEATHSTRIKE = 0x802E
    DEATHTOUCH = 0x802F
    DEATHPROOF = 0x8030
    DEATH_WARD = 0x8031
    ZOMBIESTRIKE = 0x8032
    ZOMBIETOUCH = 0x8033
    ZOMBIEPROOF = 0x8034
    ZOMBIE_WARD = 0x8035
    STONESTRIKE = 0x8036
    STONETOUCH = 0x8037
    STONEPROOF = 0x8038
    STONE_WARD = 0x8039
    POISONSTRIKE = 0x803A
    POISONTOUCH = 0x803B
    POISONPROOF = 0x803C
    POISON_WARD = 0x803D
    SLEEPSTRIKE = 0x803E
    SLEEPTOUCH = 0x803F
    SLEEPPROOF = 0x8040
    SLEEP_WARD = 0x8041
    SILENCESTRIKE = 0x8042
    SILENCETOUCH = 0x8043
    SILENCEPROOF = 0x8044
    SILENCE_WARD = 0x8045
    DARKSTRIKE = 0x8046
    DARKTOUCH = 0x8047
    DARKPROOF = 0x8048
    DARK_WARD = 0x8049
    SLOWSTRIKE = 0x804A
    SLOWTOUCH = 0x804B
    SLOWPROOF = 0x804C
    SLOW_WARD = 0x804D
    CONFUSEPROOF = 0x804E
    CONFUSE_WARD = 0x804F
    BERSERKPROOF = 0x8050
    BERSERK_WARD = 0x8051
    CURSEPROOF = 0x8052
    AUTO_SHELL = 0x8054
    AUTO_PROTECT = 0x8055
    AUTO_HASTE = 0x8056
    AUTO_REGEN = 0x8057
    AUTO_REFLECT = 0x8058
    SOS_SHELL = 0x8059
    SOS_PROTECT = 0x805A
    SOS_HASTE = 0x805B
    SOS_REGEN = 0x805C
    SOS_REFLECT = 0x805D
    SOS_NULTIDE = 0x805E
    SOS_NULFROST = 0x805F
    SOS_NULSHOCK = 0x8060
    SOS_NULBLAZE = 0x8061
    STRENGTH_3 = 0x8062
    STRENGTH_5 = 0x8063
    STRENGTH_10 = 0x8064
    STRENGTH_20 = 0x8065
    MAGIC_3 = 0x8066
    MAGIC_5 = 0x8067
    MAGIC_10 = 0x8068
    MAGIC_20 = 0x8069
    DEFENSE_3 = 0x806A
    DEFENSE_5 = 0x806B
    DEFENSE_10 = 0x806C
    DEFENSE_20 = 0x806D
    MAGIC_DEF_3 = 0x806E
    MAGIC_DEF_5 = 0x806F
    MAGIC_DEF_10 = 0x8070
    MAGIC_DEF_20 = 0x8071
    HP_5 = 0x8072
    HP_10 = 0x8073
    HP_20 = 0x8074
    HP_30 = 0x8075
    MP_5 = 0x8076
    MP_10 = 0x8077
    MP_20 = 0x8078
    MP_30 = 0x8079
    DISTILL_POWER = 0x807C
    DISTILL_MANA = 0x807D
    DISTILL_SPEED = 0x807E
    DISTILL_ABILITY = 0x807F
    BREAK_DAMAGE_LIMIT = 0x8080

def AutoAbility2Name(ability: AutoAbilities) -> str:
    match ability:
        case AutoAbilities.SENSOR: return "Sensor"
        case AutoAbilities.FIRST_STRIKE: return "First Strike"
        case AutoAbilities.INITIATIVE: return "Initiative"
        case AutoAbilities.COUNTERATTACK: return "Counterattack"
        case AutoAbilities.EVADE_AND_COUNTER: return "Evade & Counter"
        case AutoAbilities.MAGIC_COUNTER: return "Magic Counter"
        case AutoAbilities.MAGIC_BOOSTER: return "Magic Booster"
        case AutoAbilities.ALCHEMY: return "Alchemy"
        case AutoAbilities.AUTO_POTION: return "Auto-Potion"
        case AutoAbilities.AUTO_MED: return "Auto-Med"
        case AutoAbilities.AUTO_PHOENIX: return "Auto-Phoenix"
        case AutoAbilities.PIERCING: return "Piercing"
        case AutoAbilities.HALF_MP_COST: return "Half MP Cost"
        case AutoAbilities.ONE_MP_COST: return "One MP Cost"
        case AutoAbilities.DOUBLE_OVERDRIVE: return "Double Override"
        case AutoAbilities.TRIPLE_OVERDRIVE: return "Triple Overdrive"
        case AutoAbilities.SOS_OVERDRIVE: return "SOS Overdrive"
        case AutoAbilities.OVERDRIVE_TO_AP: return "Overdrive → AP"
        case AutoAbilities.DOUBLE_AP: return "Double AP"
        case AutoAbilities.TRIPLE_AP: return "Triple AP"
        case AutoAbilities.PICKPOCKET: return "Pickpocket"
        case AutoAbilities.MASTER_THIEF: return "Master Thief"
        case AutoAbilities.BREAK_HP_LIMIT: return "Break HP Limit"
        case AutoAbilities.BREAK_MP_LIMIT: return "Break MP Limit"
        case AutoAbilities.RIBBON: return "Ribbon"
        case AutoAbilities.GILLIONAIRE: return "Gillionaire"
        case AutoAbilities.HP_STROLL: return "HP Stroll"
        case AutoAbilities.MP_STROLL: return "MP Stroll"
        case AutoAbilities.NO_ENCOUNTERS: return "No Encounters"
        case AutoAbilities.FIRESTRIKE: return "Firestrike"
        case AutoAbilities.FIRE_WARD: return "Fire Ward"
        case AutoAbilities.FIREPROOF: return "Fireproof"
        case AutoAbilities.FIRE_EATER: return "Fire Eater"
        case AutoAbilities.ICESTRIKE: return "Icestrike"
        case AutoAbilities.ICE_WARD: return "Ice Ward"
        case AutoAbilities.ICEPROOF: return "Iceproof"
        case AutoAbilities.ICE_EATER: return "Ice Eater"
        case AutoAbilities.LIGHTNINGSTRIKE: return "Lightningstrike"
        case AutoAbilities.LIGHTNING_WARD: return "Lightning Ward"
        case AutoAbilities.LIGHTNINGPROOF: return "Lightningproof"
        case AutoAbilities.LIGHTNING_EATER: return "Lightning Eater"
        case AutoAbilities.WATERSTRIKE: return "Waterstrike"
        case AutoAbilities.WATER_WARD: return "Water Ward"
        case AutoAbilities.WATERPROOF: return "Waterproof"
        case AutoAbilities.WATER_EATER: return "Water Eater"
        case AutoAbilities.DEATHSTRIKE: return "Deathstrike"
        case AutoAbilities.DEATHTOUCH: return "Deathtouch"
        case AutoAbilities.DEATHPROOF: return "Deathproof"
        case AutoAbilities.DEATH_WARD: return "Death Ward"
        case AutoAbilities.ZOMBIESTRIKE: return "Zombiestrike"
        case AutoAbilities.ZOMBIETOUCH: return "Zombietouch"
        case AutoAbilities.ZOMBIEPROOF: return "Zombieproof"
        case AutoAbilities.ZOMBIE_WARD: return "Zombie Ward"
        case AutoAbilities.STONESTRIKE: return "Stonestrike"
        case AutoAbilities.STONETOUCH: return "Stonetouch"
        case AutoAbilities.STONEPROOF: return "Stoneproof"
        case AutoAbilities.STONE_WARD: return "Stone Ward"
        case AutoAbilities.POISONSTRIKE: return "Poisonstrike"
        case AutoAbilities.POISONTOUCH: return "Poisontouch"
        case AutoAbilities.POISONPROOF: return "Poisonproof"
        case AutoAbilities.POISON_WARD: return "Poison Ward"
        case AutoAbilities.SLEEPSTRIKE: return "Sleepstrike"
        case AutoAbilities.SLEEPTOUCH: return "Sleeptouch"
        case AutoAbilities.SLEEPPROOF: return "Sleepproof"
        case AutoAbilities.SLEEP_WARD: return "Sleep Ward"
        case AutoAbilities.SILENCESTRIKE: return "Silencestrike"
        case AutoAbilities.SILENCETOUCH: return "Silencetouch"
        case AutoAbilities.SILENCEPROOF: return "Silenceproof"
        case AutoAbilities.SILENCE_WARD: return "Silence Ward"
        case AutoAbilities.DARKSTRIKE: return "Darkstrike"
        case AutoAbilities.DARKTOUCH: return "Darktouch"
        case AutoAbilities.DARKPROOF: return "Darkproof"
        case AutoAbilities.DARK_WARD: return "Dark Ward"
        case AutoAbilities.SLOWSTRIKE: return "Slowstrike"
        case AutoAbilities.SLOWTOUCH: return "Slowtouch"
        case AutoAbilities.SLOWPROOF: return "Slowproof"
        case AutoAbilities.SLOW_WARD: return "Slow Ward"
        case AutoAbilities.CONFUSEPROOF: return "Confuseproof"
        case AutoAbilities.CONFUSE_WARD: return "Confuse Ward"
        case AutoAbilities.BERSERKPROOF: return "Berserkproof"
        case AutoAbilities.BERSERK_WARD: return "Berserk Ward"
        case AutoAbilities.CURSEPROOF: return "Curseproof"
        case AutoAbilities.AUTO_SHELL: return "Auto-Shell"
        case AutoAbilities.AUTO_PROTECT: return "Auto-Protect"
        case AutoAbilities.AUTO_HASTE: return "Auto-Haste"
        case AutoAbilities.AUTO_REGEN: return "Auto-Regen"
        case AutoAbilities.AUTO_REFLECT: return "Auto-Reflect"
        case AutoAbilities.SOS_SHELL: return "SOS Shell"
        case AutoAbilities.SOS_PROTECT: return "SOS Protect"
        case AutoAbilities.SOS_HASTE: return "SOS Haste"
        case AutoAbilities.SOS_REGEN: return "SOS Regen"
        case AutoAbilities.SOS_REFLECT: return "SOS Reflect"
        case AutoAbilities.SOS_NULTIDE: return "SOS NulTide"
        case AutoAbilities.SOS_NULFROST: return "SOS NulFrost"
        case AutoAbilities.SOS_NULSHOCK: return "SOS NulShock"
        case AutoAbilities.SOS_NULBLAZE: return "SOS NulBlaze"
        case AutoAbilities.STRENGTH_3: return "Strength +3%"
        case AutoAbilities.STRENGTH_5: return "Strength +5%"
        case AutoAbilities.STRENGTH_10: return "Strength +10%"
        case AutoAbilities.STRENGTH_20: return "Strength +20%"
        case AutoAbilities.MAGIC_3: return "Magic +3%"
        case AutoAbilities.MAGIC_5: return "Magic +5%"
        case AutoAbilities.MAGIC_10: return "Magic +10%"
        case AutoAbilities.MAGIC_20: return "Magic +20%"
        case AutoAbilities.DEFENSE_3: return "Defense +3%"
        case AutoAbilities.DEFENSE_5: return "Defense +5%"
        case AutoAbilities.DEFENSE_10: return "Defense +10%"
        case AutoAbilities.DEFENSE_20: return "Defense +20%"
        case AutoAbilities.MAGIC_DEF_3: return "Magic Def +3%"
        case AutoAbilities.MAGIC_DEF_5: return "Magic Def +5%"
        case AutoAbilities.MAGIC_DEF_10: return "Magic Def +10%"
        case AutoAbilities.MAGIC_DEF_20: return "Magic Def +20%"
        case AutoAbilities.HP_5: return "HP +5%"
        case AutoAbilities.HP_10: return "HP +10%"
        case AutoAbilities.HP_20: return "HP +20%"
        case AutoAbilities.HP_30: return "HP +30%"
        case AutoAbilities.MP_5: return "MP +5%"
        case AutoAbilities.MP_10: return "MP +10%"
        case AutoAbilities.MP_20: return "MP +20%"
        case AutoAbilities.MP_30: return "MP +30%"
        case AutoAbilities.DISTILL_POWER: return "Distill Power"
        case AutoAbilities.DISTILL_MANA: return "Distill Mana"
        case AutoAbilities.DISTILL_SPEED: return "Distill Speed"
        case AutoAbilities.DISTILL_ABILITY: return "Distill Ability"
        case AutoAbilities.BREAK_DAMAGE_LIMIT: return "Break Damage Limit"

def Name2AutoAbility(name: str) -> AutoAbilities:
    match name:
        case "Sensor": return AutoAbilities.SENSOR
        case "First Strike": return AutoAbilities.FIRST_STRIKE
        case "Initiative": return AutoAbilities.INITIATIVE
        case "Counterattack": return AutoAbilities.COUNTERATTACK
        case "Evade & Counter": return AutoAbilities.EVADE_AND_COUNTER
        case "Magic Counter": return AutoAbilities.MAGIC_COUNTER
        case "Magic Booster": return AutoAbilities.MAGIC_BOOSTER
        case "Alchemy": return AutoAbilities.ALCHEMY
        case "Auto-Potion": return AutoAbilities.AUTO_POTION
        case "Auto-Med": return AutoAbilities.AUTO_MED
        case "Auto-Phoenix": return AutoAbilities.AUTO_PHOENIX
        case "Piercing": return AutoAbilities.PIERCING
        case "Half MP Cost": return AutoAbilities.HALF_MP_COST
        case "One MP Cost": return AutoAbilities.ONE_MP_COST
        case "Double Override": return AutoAbilities.DOUBLE_OVERDRIVE
        case "Triple Overdrive": return AutoAbilities.TRIPLE_OVERDRIVE
        case "SOS Overdrive": return AutoAbilities.SOS_OVERDRIVE
        case "Overdrive → AP": return AutoAbilities.OVERDRIVE_TO_AP
        case "Double AP": return AutoAbilities.DOUBLE_AP
        case "Triple AP": return AutoAbilities.TRIPLE_AP
        case "Pickpocket": return AutoAbilities.PICKPOCKET
        case "Master Thief": return AutoAbilities.MASTER_THIEF
        case "Break HP Limit": return AutoAbilities.BREAK_HP_LIMIT
        case "Break MP Limit": return AutoAbilities.BREAK_MP_LIMIT
        case "Ribbon": return AutoAbilities.RIBBON
        case "Gillionaire": return AutoAbilities.GILLIONAIRE
        case "HP Stroll": return AutoAbilities.HP_STROLL
        case "MP Stroll": return AutoAbilities.MP_STROLL
        case "No Encounters": return AutoAbilities.NO_ENCOUNTERS
        case "Firestrike": return AutoAbilities.FIRESTRIKE
        case "Fire Ward": return AutoAbilities.FIRE_WARD
        case "Fireproof": return AutoAbilities.FIREPROOF
        case "Fire Eater": return AutoAbilities.FIRE_EATER
        case "Icestrike": return AutoAbilities.ICESTRIKE
        case "Ice Ward": return AutoAbilities.ICE_WARD
        case "Iceproof": return AutoAbilities.ICEPROOF
        case "Ice Eater": return AutoAbilities.ICE_EATER
        case "Lightningstrike": return AutoAbilities.LIGHTNINGSTRIKE
        case "Lightning Ward": return AutoAbilities.LIGHTNING_WARD
        case "Lightningproof": return AutoAbilities.LIGHTNINGPROOF
        case "Lightning Eater": return AutoAbilities.LIGHTNING_EATER
        case "Waterstrike": return AutoAbilities.WATERSTRIKE
        case "Water Ward": return AutoAbilities.WATER_WARD
        case "Waterproof": return AutoAbilities.WATERPROOF
        case "Water Eater": return AutoAbilities.WATER_EATER
        case "Deathstrike": return AutoAbilities.DEATHSTRIKE
        case "Deathtouch": return AutoAbilities.DEATHTOUCH
        case "Deathproof": return AutoAbilities.DEATHPROOF
        case "Death Ward": return AutoAbilities.DEATH_WARD
        case "Zombiestrike": return AutoAbilities.ZOMBIESTRIKE
        case "Zombietouch": return AutoAbilities.ZOMBIETOUCH
        case "Zombieproof": return AutoAbilities.ZOMBIEPROOF
        case "Zombie Ward": return AutoAbilities.ZOMBIE_WARD
        case "Stonestrike": return AutoAbilities.STONESTRIKE
        case "Stonetouch": return AutoAbilities.STONETOUCH
        case "Stoneproof": return AutoAbilities.STONEPROOF
        case "Stone Ward": return AutoAbilities.STONE_WARD
        case "Poisonstrike": return AutoAbilities.POISONSTRIKE
        case "Poisontouch": return AutoAbilities.POISONTOUCH
        case "Poisonproof": return AutoAbilities.POISONPROOF
        case "Poison Ward": return AutoAbilities.POISON_WARD
        case "Sleepstrike": return AutoAbilities.SLEEPSTRIKE
        case "Sleeptouch": return AutoAbilities.SLEEPTOUCH
        case "Sleepproof": return AutoAbilities.SLEEPPROOF
        case "Sleep Ward": return AutoAbilities.SLEEP_WARD
        case "Silencestrike": return AutoAbilities.SILENCESTRIKE
        case "Silencetouch": return AutoAbilities.SILENCETOUCH
        case "Silenceproof": return AutoAbilities.SILENCEPROOF
        case "Silence Ward": return AutoAbilities.SILENCE_WARD
        case "Darkstrike": return AutoAbilities.DARKSTRIKE
        case "Darktouch": return AutoAbilities.DARKTOUCH
        case "Darkproof": return AutoAbilities.DARKPROOF
        case "Dark Ward": return AutoAbilities.DARK_WARD
        case "Slowstrike": return AutoAbilities.SLOWSTRIKE
        case "Slowtouch": return AutoAbilities.SLOWTOUCH
        case "Slowproof": return AutoAbilities.SLOWPROOF
        case "Slow Ward": return AutoAbilities.SLOW_WARD
        case "Confuseproof": return AutoAbilities.CONFUSEPROOF
        case "Confuse Ward": return AutoAbilities.CONFUSE_WARD
        case "Berserkproof": return AutoAbilities.BERSERKPROOF
        case "Berserk Ward": return AutoAbilities.BERSERK_WARD
        case "Curseproof": return AutoAbilities.CURSEPROOF
        case "Auto-Shell": return AutoAbilities.AUTO_SHELL
        case "Auto-Protect": return AutoAbilities.AUTO_PROTECT
        case "Auto-Haste": return AutoAbilities.AUTO_HASTE
        case "Auto-Regen": return AutoAbilities.AUTO_REGEN
        case "Auto-Reflect": return AutoAbilities.AUTO_REFLECT
        case "SOS Shell": return AutoAbilities.SOS_SHELL
        case "SOS Protect": return AutoAbilities.SOS_PROTECT
        case "SOS Haste": return AutoAbilities.SOS_HASTE
        case "SOS Regen": return AutoAbilities.SOS_REGEN
        case "SOS Reflect": return AutoAbilities.SOS_REFLECT
        case "SOS NulTide": return AutoAbilities.SOS_NULTIDE
        case "SOS NulFrost": return AutoAbilities.SOS_NULFROST
        case "SOS NulShock": return AutoAbilities.SOS_NULSHOCK
        case "SOS NulBlaze": return AutoAbilities.SOS_NULBLAZE
        case "Strength +3%": return AutoAbilities.STRENGTH_3
        case "Strength +5%": return AutoAbilities.STRENGTH_5
        case "Strength +10%": return AutoAbilities.STRENGTH_10
        case "Strength +20%": return AutoAbilities.STRENGTH_20
        case "Magic +3%": return AutoAbilities.MAGIC_3
        case "Magic +5%": return AutoAbilities.MAGIC_5
        case "Magic +10%": return AutoAbilities.MAGIC_10
        case "Magic +20%": return AutoAbilities.MAGIC_20
        case "Defense +3%": return AutoAbilities.DEFENSE_3
        case "Defense +5%": return AutoAbilities.DEFENSE_5
        case "Defense +10%": return AutoAbilities.DEFENSE_10
        case "Defense +20%": return AutoAbilities.DEFENSE_20
        case "Magic Def +3%": return AutoAbilities.MAGIC_DEF_3
        case "Magic Def +5%": return AutoAbilities.MAGIC_DEF_5
        case "Magic Def +10%": return AutoAbilities.MAGIC_DEF_10
        case "Magic Def +20%": return AutoAbilities.MAGIC_DEF_20
        case "HP +5%": return AutoAbilities.HP_5
        case "HP +10%": return AutoAbilities.HP_10
        case "HP +20%": return AutoAbilities.HP_20
        case "HP +30%": return AutoAbilities.HP_30
        case "MP +5%": return AutoAbilities.MP_5
        case "MP +10%": return AutoAbilities.MP_10
        case "MP +20%": return AutoAbilities.MP_20
        case "MP +30%": return AutoAbilities.MP_30
        case "Distill Power": return AutoAbilities.DISTILL_POWER
        case "Distill Mana": return AutoAbilities.DISTILL_MANA
        case "Distill Speed": return AutoAbilities.DISTILL_SPEED
        case "Distill Ability": return AutoAbilities.DISTILL_ABILITY
        case "Break Damage Limit": return AutoAbilities.BREAK_DAMAGE_LIMIT