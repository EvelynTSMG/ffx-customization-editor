import struct

class BinFile:
    unk1: int
    unk2: int
    unk3: int
    block_size: int
    blocks: list

    def getBlockCount(self) -> int:
        return len(self.blocks) - 1

    def getTotalBlockSize(self) -> int:
        return len(self.blocks) * self.block_size
    
    def getBlocksStartAddress(self) -> int:
        return 0x14
    
    def getHeader(self):
        return struct.pack("<IIHHHHI", self.unk1, self.unk2, self.unk3, self.getBlockCount(),
                    self.block_size, self.getTotalBlockSize(), self.getBlocksStartAddress())

    def readHeaderFromBin(self, byts):
        (self.unk1, self.unk2, self.unk3) = struct.unpack("<IIH", byts[:0xa])
    
    def readHeaderFromJson(self, inJson):
        self.unk1 = inJson["unk1"]
        self.unk2 = inJson["unk2"]
        self.unk3 = inJson["unk3"]
    
    def saveHeaderToJson(self, outJson: dict):
        outJson["unk1"] = self.unk1
        outJson["unk2"] = self.unk2
        outJson["unk3"] = self.unk3
    
    def saveToBin(self, filepath) -> None:
        return NotImplemented

    def saveToJson(self, filepath) -> None:
        return NotImplemented

    def loadFromBin(self, filepath) -> None:
        return NotImplemented

    def loadFromJson(self, filepath) -> None:
        return NotImplemented

INDENT = 2
ITEM_COUNT = 112